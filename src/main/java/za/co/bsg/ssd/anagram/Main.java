package za.co.bsg.ssd.anagram;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import za.co.bsg.ssd.anagram.counter.AnagramCounter;
import za.co.bsg.ssd.anagram.printer.AnagramPrinter;
import za.co.bsg.ssd.anagram.reader.WordList;

public class Main implements WordList, AnagramCounter, AnagramPrinter {

    public static void main(String[] args) {
        /**
         * Provide implementations for {@link WordList}, {@link AnagramCounter}, and {@link AnagramPrinter}.
         * Then replace the nulls in the instantiation below with your implementations.
         */
        WordList wordList = new Main();
        AnagramPrinter anagramPrinter = new Main();
        AnagramCounter anagramCounter = new Main();

        AnagramCalculator anagramCalculator = new AnagramCalculator(wordList, anagramCounter, anagramPrinter);

        try {
            anagramCalculator.run();
        } catch (NullPointerException e) {
            System.err.println("Did you forget to provide your implementations to the AnagramCalculator?");
            throw e;
        }
    }
    
    @Override
    public List<String> readAll(){
        List<String> result = new ArrayList<>();
        ClassLoader cl = getClass().getClassLoader();
        File file = new File(cl.getResource("british-english.txt").getFile());
        try (Scanner scanner = new Scanner(file)){
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                result.add(line.toLowerCase());
            }
        } catch(IOException ex){
            ex.printStackTrace();
        }     
        return result;
    }
    
    Map<String, List<String>> findAnagrams(List<String> words){
        // takes a word list and finds all the anagrams that are in this word list
        Map<String, List<String>> result = new HashMap<>(); // a Map to hold the results
        // iterate through each word in the word list, sort the letters of the word and check if the key is already entered into the Map if it is we have an anagram
        for(String item : words){ 
            char[] chars= item.toCharArray(); // takes the word and puts it in a char array to sort
            Arrays.sort(chars);
            StringBuilder s = new StringBuilder();
            for(char c : chars)
                //iterate through the char array and add the letter to a string builder
                s.append(c);
            String key = s.toString();
            if(!result.containsKey(key)){
                result.put(key, new ArrayList<>()); // if the sorted letters does not exisit we have a new possible anagram
            }
            if(!(result.get(key).contains(item)))
                result.get(key).add(item); // the key exisits so we check if the list contains the word already to prevent eg Booth and booth from being added twice.
        }
        return result;
    }

    @Override
    public Map<Integer, Integer> countAnagramsPerWordLength(List<String> words) {
        Map result = new HashMap(); // Map to hold the results
        Map aMap = findAnagrams(words); // gets a Map with a key as the sorted letters of a word and the list of words that the dictionary contains with those letters in the key
        Iterator<Map.Entry<String, List<String>>> entries = aMap.entrySet().iterator(); // an iterator for each element in the Map
        // iterate through each element in the Map and put the word length and anagram count into the result Map
        while (entries.hasNext()) {
         Map.Entry<String, List<String>> entry = entries.next(); // gets the next entry in the Map
         int keySize = entry.getKey().length(); // size of the key which is the size of all the words for this key
         int valueCount = entry.getValue().size(); // counts the number of anagrams for the particular key
         if(entry.getValue().size() > 1){ // does not take entries with only 1 value in the list of words
            if(result.containsKey(keySize))
                result.put(keySize, (Integer)result.get(keySize) + valueCount); // if there is already a word length for this key then add the number to the total
             else
                result.put(keySize, valueCount); // there is no entry for this word length so create one with a new value
         }
        }    
        return result;
    }

    @Override
    public void print(Map<Integer, Integer> results) {
        // takes a Map of word length and count of anagrams and displays the results to the console
        ArrayList<Integer> keys = new ArrayList<>(results.keySet());
        String out = ""; // result string to build to
        // iterate through the Map and build the string with the results
        for(int i = keys.size()-1; i >=0;i--){ 
            out = out + (String.format("For words '%1$2d' characters long there are '%2$4d' anagrams.\n", keys.get(i), results.get(keys.get(i))));
        }
        //print the output to the console
        System.out.print(out);
    }
    
}
