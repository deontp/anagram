/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.bsg.ssd.anagram;

import static java.util.Arrays.asList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author deontp
 */
public class MainTest {
    
    public MainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    /**
     * Test of findAnagrams method, of class Main.
     */
    @Test
    public void testFindAnagrams() {
        System.out.println("findAnagrams");
        List<String> words = asList("friend", "refind", "finder", "monster", "mentors", "two");
        Main instance = new Main();
        Map<String, List<String>> expResult = new HashMap();
        expResult.put("definr", asList("friend", "refind", "finder"));
        expResult.put("emnorst", asList("monster","mentors"));
        expResult.put("otw", asList("two"));
        Map<String, List<String>> result = instance.findAnagrams(words);
        assertEquals(expResult, result);
    }

    /**
     * Test of countAnagramsPerWordLength method, of class Main.
     */
    @Test
    public void testCountAnagramsPerWordLength() {
        List<String> words = asList("friend", "refind", "finder", "monster", "mentors", "two");
        System.out.println("countAnagramsPerWordLength");
        Main instance = new Main();
        Map<Integer, Integer> expResult = new HashMap();
        expResult.put(6,3);
        expResult.put(7,2);
        Map<Integer, Integer> result = instance.countAnagramsPerWordLength(words);
        assertEquals(expResult, result);
    }    
}
